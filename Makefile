.PHONY: all install clean uninstall home-install home-uninstall xdg-install xdg-uninstall email mpd nethack screen terminal vimb dunst ratpoison twm xorg

BINDIR = ${HOME}/.local/bin

emacs-eval=emacs -Q --batch --load ob-tangle --eval

all: email mpd nethack screen terminal vimb dunst ratpoison xorg twm

clean:
	rm -rf xdg-config out

install: home-install xdg-install bin-install

home-install:
	stow -d out -t ${HOME} .

xdg-install:
	stow -d xdg-config -t ${XDG_CONFIG_HOME} .

bin-install:
	stow -d bin -t ${BINDIR} .

uninstall: home-uninstall xdg-uninstall bin-uninstall

home-uninstall:
	stow -d out -D -t ${HOME} .

xdg-uninstall:
	stow -d xdg-config -D -t ${XDG_CONFIG_HOME} .

bin-uninstall:
	stow -d bin -D -t ${BINDIR} .

EMAILFILES = out/.local/share/mail/.notmuch/hooks/pre-new xdg-config/mbsync/config xdg-config/msmtp/config xdg-config/notmuch/config
email : ${EMAILFILES}
MPDFILES = xdg-config/mpd/mpd.conf
mpd : ${MPDFILES}
NETHACKFILES = xdg-config/nethack/nethackrc bin/nh
nethack : ${NETHACKFILES}
SCREENFILES = xdg-config/screen/screenrc
screen : ${SCREENFILES}
TERMINALFILES = out/.bash_profile out/.bashrc xdg-config/readline/inputrc
terminal : ${TERMINALFILES}
VIMBFILES = xdg-config/vimb/config xdg-config/vimb/style.css
vimb : ${VIMBFILES}
DUNSTFILES = xdg-config/dunst/dunstrc
dunst : ${DUNSTFILES}
RATPOISONFILES = xdg-config/ratpoison/config bin/ratborder bin/dratmenu
ratpoison : ${RATPOISONFILES}
TWMFILES = xdg-config/twm/config
twm : ${TWMFILES}
XORGFILES = out/.xinitrc bin/ratpoisonrc bin/twmrc bin/exwm xdg-config/sxhkd/sxhkdrc
xorg: ${XORGFILES}

${EMAILFILES}: email.org
	mkdir -p ${XDG_CONFIG_HOME}/{notmuch,mbsync,msmtp}
	${emacs-eval} '(org-babel-tangle-file "email.org")'

${MPDFILES}: mpd.org
	mkdir -p ${XDG_CONFIG_HOME}/mpd ~/.local/share/mpd/playlists
	${emacs-eval} '(org-babel-tangle-file "mpd.org")'

${NETHACKFILES}: nethack.org
	mkdir -p ${XDG_CONFIG_HOME}/nethack
	${emacs-eval} '(org-babel-tangle-file "nethack.org")'

${SCREENFILES}: screen.org
	mkdir -p ${XDG_CONFIG_HOME}/screen
	${emacs-eval} '(org-babel-tangle-file "screen.org")'

${TERMINALFILES}: terminal.org
	mkdir -p ${XDG_CONFIG_HOME}/{bash,readline}
	${emacs-eval} '(org-babel-tangle-file "terminal.org")'

${VIMBFILES}: vimb.org
	mkdir -p ${XDG_CONFIG_HOME}/vimb
	${emacs-eval} '(org-babel-tangle-file "vimb.org")'

${DUNSTFILES}: dunst.org
	mkdir -p ${XDG_CONFIG_HOME}/dunst
	${emacs-eval} '(org-babel-tangle-file "dunst.org")'

${RATPOISONFILES}: ratpoison.org
	mkdir -p ${XDG_CONFIG_HOME}/ratpoison
	${emacs-eval} '(org-babel-tangle-file "ratpoison.org")'

${TWMFILES}: twm.org
	mkdir -p ${XDG_CONFIG_HOME}/twm
	${emacs-eval} '(org-babel-tangle-file "twm.org")'

${XORGFILES}: xorg.org
	mkdir -p ${XDG_CONFIG_HOME}/sxhkd
	${emacs-eval} '(org-babel-tangle-file "xorg.org")'
