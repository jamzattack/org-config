# -*- org-use-property-inheritance: t; -*-
#+TITLE: Email config - mbysnc, msmtp, notmuch

* mbsync
:PROPERTIES:
:header-args:  :tangle "xdg-config/mbsync/config" :mkdirp yes
:END:

MBSYNC (ISYNC) is the program that downloads all mail. The
configuration is whitespace-dependent, so I have split it into the
proper sections.

** IMAP Account

This section describes the account used

#+begin_src conf-space
IMAPAccount gmail
Host imap.gmail.com
User beardsleejamie@gmail.com
PassCmd "pass gmail"
SSLType IMAPS
CertificateFile /etc/ssl/certs/ca-certificates.crt
#+end_src

** IMAP Store

#+begin_src conf-space
IMAPStore gmail-remote
Account gmail
#+end_src

** Maildir Store

#+begin_src conf-space
MaildirStore gmail-local
Subfolders Verbatim
Path ~/.local/share/mail/
Inbox ~/.local/share/mail/Inbox
#+end_src

** Putting it all together

#+begin_src conf-space
Channel gmail
Master :gmail-remote:
Slave :gmail-local:
Patterns * ![Gmail]* "[Gmail]/Sent Mail" "[Gmail]/Starred" "[Gmail]/All Mail"
Create Both
SyncState *
#+end_src

* notmuch
:PROPERTIES:
:header-args:  :tangle "xdg-config/notmuch/config"
:header-args+: :mkdirp t
:END:

NOTMUCH is the program that searches and indexes all local mail. It
has a nice emacs interface.

** database

The only value supported here is 'path' which should be the top-level
directory where your mail currently exists and to where mail will be
delivered in the future. Files should be individual email messages.
Notmuch will store its database within a sub-directory of the path
configured here named ".notmuch".

#+begin_src conf-unix
  [database]
  path=/home/jdb/.local/share/mail
#+end_src

** user

Here is where you can let notmuch know how you would like to be
addressed. Valid settings are

name		Your full name.
primary_email	Your primary email address.
other_email	A list (separated by ';') of other email addresses
at which you receive email.

Notmuch will use the various email addresses configured here when
formatting replies. It will avoid including your own addresses in the
recipient list of replies, and will set the From address based on the
address to which the original email was addressed.

#+begin_src conf-unix
  [user]
  name=Jamie Beardslee
  primary_email=beardsleejamie@gmail.com
#+end_src

** Configuration for "notmuch new"

The following options are supported here:

tags	A list (separated by ';') of the tags that will be
added to all messages incorporated by "notmuch new".

ignore	A list (separated by ';') of file and directory names
that will not be searched for messages by "notmuch new".

#+begin_src conf-unix
  [new]
  tags=unread;inbox;
  ignore=
#+end_src

** Search configuration

The following option is supported here:

exclude_tags
A ;-separated list of tags that will be excluded from
search results by default.  Using an excluded tag in a
query will override that exclusion.

#+begin_src conf-unix
  [search]
  exclude_tags=deleted;spam;
#+end_src

** Maildir compatibility

The following option is supported here:

synchronize_flags      Valid values are true and false.

#+begin_src conf-unix
  [maildir]
  synchronize_flags=true
#+end_src

** hooks

*** Update database

The following is a hook to update the mail database with [[*mbsync][mbsync]] before
invoking =notmuch new=.

#+begin_src sh :shebang "#!/bin/sh" :tangle "out/.local/share/mail/.notmuch/hooks/pre-new"
  mbsync -a -c "$XDG_CONFIG_HOME/mbsync/config"; true
#+end_src

*** Add tags

The post-new hook should tag all mail appropriately.  I use a =list=
tag for mailing lists, and a =uni= tag for university emails.

I also make sure that my sent mail is properly tagged as =sent=.  This
is achieved in the post-insert hook as well, but that doesn't work if
I send an email any way other than via notmuch.

#+begin_src sh :shebang "#!/bin/sh" :tangle "out/.local/share/mail/.notmuch/hooks/post-new"
  # Mailing lists -- remove them from inbox
  notmuch tag +list +emacs -- "/emacs.*@gnu\.org"
  notmuch tag +list +lilypond -- "/lilypond.*/@gnu\.org"
  notmuch tag -inbox -- tag:list

  # University -- keep in inbox, but tag with "uni"
  notmuch tag +uni -- "*vuw.ac.nz"
  notmuch tag +uni -- "*wgtn.ac.nz"

  # Tag sent mail as sent
  notmuch tag +sent -- from:"$EMAIL"

  # If unread mail exists and Xorg is running, send a notification
  if [ -n "$(notmuch search tag:unread AND tag:inbox)" ]  ; then
      [ -n "$DISPLAY" ] && notify-send "You've got mail!"
  fi
#+end_src

#+begin_src sh :shebang "#!/bin/sh" :tangle "out/.local/share/mail/.notmuch/hooks/post-insert"
  # Tag sent mail as sent
  notmuch tag +sent -- from:"$EMAIL"
#+end_src

* msmtp
:PROPERTIES:
:header-args:  :tangle "xdg-config/msmtp/config"
:header-args+: :mkdirp t
:END:

MSMTP is the program that sends mail.

** defaults

Set default values for all accounts

#+begin_src conf-space
  defaults
  tls_trust_file /etc/ssl/certs/ca-certificates.crt
#+end_src

*** Use the mail submission port 587 instead of the SMTP port 25.

#+begin_src conf-space
  port 587
#+end_src

*** TLS settings

#+begin_src conf-space
  tls on
  tls_starttls on
  tls_certcheck off
#+end_src

** accounts

I only use one email, but you can set multiple here.

*** gmail

The account name and host

#+begin_src conf-space
  account gmail
  host smtp.gmail.com
  port 587
#+end_src

**** address and user

#+begin_src conf-space
  from beardsleejamie@gmail.com
  user beardsleejamie
#+end_src

**** password

Similar to mbsync, you can specify a command to get the password
rather than write in the config file itself.

#+begin_src conf-space
  auth plain
  passwordeval "pass gmail"
#+end_src

*** University

The account name and host

#+begin_src conf-space
  account uni
  host smtp.office265.com
  port 587
#+end_src

**** address and user

#+begin_src conf-space
  from beardsjami@myvuw.ac.nz
  user beardsjami@myvuw.ac.nz
#+end_src

**** password

Similar to mbsync, you can specify a command to get the password
rather than write in the config file itself.

#+begin_src conf-space
  auth plain
  passwordeval "pass university-email"
#+end_src
*** my own server

#+begin_src conf-space
  account jamzattack.xyz
  host mail.jamzattack.xyz
  port 587
#+end_src

**** address and user

#+begin_src conf-space
  from jdb@jamzattack.xyz
  user jdb
#+end_src

**** password

Similar to mbsync, you can specify a command to get the password
rather than write in the config file itself.

#+begin_src conf-space
  auth plain
  passwordeval "pass mail.jamzattack.xyz"
#+end_src
