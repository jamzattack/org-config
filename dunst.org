#+PROPERTY: header-args:conf-unix :tangle "xdg-config/dunst/dunstrc" :mkdirp t

* Global

#+begin_src conf-unix
  [global]
#+end_src


** Frame settings

#+begin_src conf-unix
  frame_width = 1
  frame_color = "#ffffff"
#+end_src
	
** font

Arch Linux - =terminus-font= package does not work, use
=terminus-font-otb=.

#+begin_src conf-unix
  font = Terminus 9
#+end_src

** size/shape

The geometry of the window:

#+begin_src conf-unix
  geometry = "0x4-10-32"
#+end_src
	
Shrink window if it's smaller than the width.  Will be ignored if width is 0.

#+begin_src conf-unix
  shrink = yes
#+end_src
	
The height of a single line.  If the height is smaller than the
font height, it will get raised to the font height.
This adds empty space above and under the text.

#+begin_src conf-unix
  line_height = 0
#+end_src
	
*** Separator

Draw a line of "separator_height" pixel height between two notifications.

#+begin_src conf-unix
  separator_height = 1
#+end_src
	
Define a color for the separator.

#+begin_src conf-unix
  separator_color = frame
#+end_src
	

*** padding

Padding between text and separator.
#+begin_src conf-unix
  padding = 5
#+end_src
	
Horizontal padding.

#+begin_src conf-unix
  horizontal_padding = 5
#+end_src
	
* Colours

IMPORTANT: colors have to be defined in quotation marks.
Otherwise the "#" and following would be interpreted as a comment.

** Low urgency

#+begin_src conf-unix
  [urgency_low]
  background = "#000000"
  foreground = "#ffffff"
  timeout = 5
#+end_src

** Normal urgency

#+begin_src conf-unix
  [urgency_normal]
  background = "#000000"
  foreground = "#ffffff"
  timeout = 10
#+end_src

** Critical urgency

#+begin_src conf-unix
  [urgency_critical]
  background = "#d35c5c"
  foreground = "#ffffff"
  timeout = 60
#+end_src

* Keybindings

#+begin_src conf-unix
  [shortcuts]
#+end_src

** Clear notifications

#+begin_src conf-unix
  close_all = mod4+Space
#+end_src
