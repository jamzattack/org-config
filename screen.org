#+TITLE: screenrc
#+PROPERTY: header-args:conf :tangle "xdg-config/screen/screenrc" :mkdirp t

* Keybindings

** Escape
I use =C-t= because it's more comfortable, and doesn't get in the way
as much.

#+begin_src conf
  escape ^Tt
#+end_src

** Splits

| S          | vertical      |
| pipe       | vertical      |
| s          | horizontal    |
| underscore | horizontal    |
|------------+---------------|
| R          | remove window |

#+begin_src conf
  bind S split -v
  bind s split
  bind | split -v
  bind _ split
  bind R remove
#+end_src

** Copy

My attempt at emacs-like editing.

#+begin_src conf
  markkeys h=^B:l=^F:$=^E:' '=^@:^M=^[w:
#+end_src

* Appearance

** Status line

I have no idea how this arcane shit works. It sets up a mode-line
style thing with the following information.

[Host] [List of Windows] [Date Time]

#+begin_src conf
  hardstatus off
  hardstatus alwayslastline '%{= kG}[ %{G}%H %{g}][%= %{= kw}%?%-Lw%?%{r}(%{W}%n*%f%t%?(%u)%?%{r})%{w}%?%+Lw%?%?%= %{g}][%{B} %m-%d %{W} %c %{g}]'
#+end_src

** Cursor

#+begin_src conf
  exec echo -ne '\e[1 q'
#+end_src

* Scrollback

** Set the default scrollback to a larger number

#+begin_src conf
  defscrollback 10000
#+end_src

** Remove blank lines

#+begin_src conf
  compacthist on
#+end_src

